import sys
from comum import *
import pickle

'''
Usage:
python metodo.py arqUsuarios beta porcentagem
'''
arqUsuarios = sys.argv[1]
try:
    beta = float(sys.argv[2])
except:
    beta = 0.5
    
try:
    porcentagem = float(sys.argv[3])
except:
    porcentagem = 0.2

'''
A leitura do arquivo de usuarios no formato
id_usuario::cidade::palavras_separadas_por_espaco::id_amigos_separados_por_espaco
'''
with open(arqUsuarios) as F:
    for line in F:
        id, cidade, palavras, amigos = line.split("::")
        palavras = set(palavras.split())
        amigos = set(amigos.split())
        addUsuario(id, cidade, amigos, palavras)

#escolhe os usuarios que serao seeds (aleatoriamente) e calcula P(w|Teta_i)
startSeed(porcentagem)

for iteracao in xrange(0,1000):
    print 'iteracao do gibbs %s' % iteracao
    for usuario in usuarios:
        if usuarios[usuario]['seed']: #se usuario for seed, nao muda cidade
            continue 
        pesos = []
        #calcula a distribuicao de acordo com o potts model e escolhe uma cidade
        for cidade in cidades.keys():
            pesos.append( (cidade,calculaPotts(usuario, cidade, beta)) )        
        cidade_escolhida = escolhe(pesos) 
        usuarios[usuario]['gibbs'].append(cidade_escolhida)

hit = 0
miss = 0
for usuario in usuarios:
    prob_maior = 0
    cidade_maior = None
    if usuarios[usuario]['seed']:
        continue
    usuarios[usuario]['probabilidades'] = {}
    for cidade in cidades:
        '''
        A probabilidade dada por gibbs refere-se a frequencia com que uma cidade foi
        escolhida durante o processo de gibbs.
        
        Seu valor, em geral, eh muito maior do que a probabilidade condicional das palavras. 
        Para evitar que ela domine, normatiza-se a probabildiade condicional das palavras em 
        relacao a uma cidade base (no caso, rio_de_janeiro) 
        '''
        prob_gibbs = len([c for c in usuarios[usuario]['gibbs'] if c==cidade])/float(len(usuarios[usuario]['gibbs']))
        
        prob_palavras = probabilidadePalavra(usuario, cidade, 'rio_de_janeiro')
        probabilidade = prob_palavras * prob_gibbs
        usuarios[usuario]['probabilidades'][cidade] = probabilidade
        
        #mantem tracking da maior probabilidade
        if probabilidade > prob_maior or cidade_maior is None:
            cidade_maior = cidade
            prob_maior = probabilidade
            
    if cidade_maior == usuarios[usuario]['cidade']:
        hit += 1
    else:
        miss +=1

precisao = float(hit)/(miss+hit)
print 'Precisa %s ' % precisao
pickle.dump(usuarios, open('resultadoGibbs.p', 'w'))