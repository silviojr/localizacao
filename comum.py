from collections import defaultdict
import random
import math
from numpy import cumsum, sort, sum, searchsorted
from numpy.random import rand


usuarios = {}
palavras = {}
cidades = defaultdict(list)
probabilidade_palavras = defaultdict(lambda:defaultdict(int))
palavras_cidade = defaultdict(int)

def addUsuario(_id, cidade, amigos=[], palavras=[]):
    usuarios[_id] = {'cidade':cidade,
                     'amigos': amigos,
                     'palavras':palavras,
                     'gibbs':[cidade], 
                     'seed': True}
    cidades[cidade].append(_id)
        


def addPalavra(_id, cidade, probabilidade):
    try:
        palavras[_id][cidade] = probabilidade
    except:
        palavras[_id] = {cidade:probabilidade}
        
'''
Escolhe de forma estratificada os usuarios de acordo com a porcentagem
'''
def startSeed(porcentagem_sem_seed):
    for cidade in cidades:
        numero = int(porcentagem_sem_seed * len(cidades[cidade]))
        usuarios_escolhidos = random.sample(cidades[cidade], numero)
        for usuario in usuarios_escolhidos:
            usuarios[usuario]['seed'] = False
            usuarios[usuario]['gibbs'] = [ random.sample(cidades.keys() , 1) ]
             
    for usuario in usuarios:
        if usuarios[usuario]['seed']:
            for palavra in usuarios[usuario]['palavras']:
                probabilidade_palavras[palavra][usuarios[usuario]['cidade']] += 1
                palavras_cidade[ usuarios[usuario]['cidade'] ] += 1
    
    for palavra in probabilidade_palavras:
        for cidade in probabilidade_palavras[palavra]:
            probabilidade_palavras[palavra][cidade] = float(probabilidade_palavras[palavra][cidade])/palavras_cidade[cidade]

def calculaPotts(usuario, cidade, beta):
    somatorio = 0
    for amigo in usuarios[usuario]['amigos']:
        try:
            if usuarios[amigo]['gibbs'][-1] == cidade:
                somatorio += 1
        except:
            pass
    return math.exp(beta * somatorio)

'''
metodo adaptado de http://glowingpython.blogspot.com.br/2012/09/weighted-random-choice.html

lista = [(cidade, peso), (cidade, peso)...]
'''
def escolhe(lista):
    t = cumsum([l[1] for l in lista ])
    s = sum( [l[1] for l in lista ])
    
    indice = searchsorted(t, rand(1)*s)[0]
    return lista[indice][0]
            
def probabilidadePalavra(usuario, cidade, cidade_base):
    produtorio = 1
    for palavra in usuarios[usuario]['palavras']:
        if probabilidade_palavras[palavra][cidade] and probabilidade_palavras[palavra][cidade_base]:
            ratio = probabilidade_palavras[palavra][cidade]/probabilidade_palavras[palavra][cidade_base]
            produtorio * ratio
    return produtorio
    
        